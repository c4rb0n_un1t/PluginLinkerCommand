#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this),
	m_cliElement(new CLIElement(this, "plugin", "[add | remove]"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(ICLIElement), m_cliElement}
	},
	{
		{INTERFACE(IPluginLinker), m_pluginLinker}
	}
	);
	connect(m_cliElement, &CLIElement::onHandle, this, &Plugin::handle);
}

void Plugin::handle(const QStringList& args)
{
	switch (args.length())
	{
	case 0: {
		auto items = m_pluginLinker->getItemsWithInterface(INTERFACE(IPlugin));

		for(auto& item : *items.toStrongRef())
		{
			auto descr = item.toStrongRef()->descr().toStrongRef();
			qDebug() << "Plugin::handle" << QString("%1: %2").arg(descr->uid()).arg(descr->name());
		}
	}
	break;

	case 2: {
		auto action = args.at(0);
		if(action == "remove")
		{
			auto pluginId = args.at(1).toUInt();
			auto plugin = m_pluginLinker->getItemByUID(pluginId);
			m_pluginLinker->removePlugin(plugin);
		} else if(action == "download")
		{
			QString url = args[1];
			m_download.download(url, "", "");
			return;
		} else if(action == "add") {
			auto pluginPath = args.at(1);
			m_pluginLinker->addPlugin(pluginPath);
		}
	}
	break;

	default:
		qDebug() << "Unknown command";
	}
}
